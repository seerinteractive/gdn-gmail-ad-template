
//TODO: wrap html in export so that * selector doesnt apply to html,head, etc on export

//TODO: apply images and teaser.txt to preview-index.html



//PRepend AUTOGNERATED disclaimer at top of preview/index.html


// Import all necessary modules
var gulp      = require( 'gulp' ),
gutil = require('gulp-util'),

    sass      = require( 'gulp-sass' ),
    inlineCss = require( 'gulp-inline-css' ),
    bs        = require( 'browser-sync' ).create(),
    sequence  = require( 'run-sequence' ),
    plumber   = require( 'gulp-plumber' ),
    rename    = require( 'gulp-rename' ),
    data      = require( 'gulp-data' ),
    swig      = require( 'gulp-swig' ),
    replace   = require('gulp-replace'),

    //htmlincludes = require('gulp-html-ssi');
    concat   = require('gulp-concat'),
    data = require('gulp-data'),

    mailer = require( 'nodemailer' ),
    fs     = require( 'fs' );

var THEMES = [
  'a-light', 'a-dark' ,
    'b-light', 'b-dark' ,
      'c-light',  'c-dark' ,
        'd-light', 'd-dark' ,
  //'NONE'
];

/*
gulp.task( 'build-css', function() {
    gulp.src( './src/styles.scss' )
        .pipe( plumber() ) // report errors w/o stopping Gulp
        .pipe( sass() )
        .pipe( gulp.dest( './build' ) );
});*/

gulp.task( 'export-html', function(cb) {

      gulp.src( './src/styles.scss' )
          .pipe( plumber() ) // report errors w/o stopping Gulp
          .pipe( sass() )
          .on('data', function(file) {
            var css = file.contents.toString();
            /*
              gulp.src( './src/ad.html' )
                  .pipe( inlineCss({
                    removeHtmlSelectors:0,//TODO
                    //url:'',
                    preserveMediaQueries:false,
                    applyWidthAttributes:true,
                    applyTableAttributes:true,
                      applyStyleTags:  true,
                      removeStyleTags: false,
                      applyLinkTags:   true,
                      removeLinkTags:  true,
                      extraCss: css
                   }))
                   .pipe( rename( 'index.html' ) )
                   .pipe( gulp.dest( './export/' ) )
              .on('end', cb);
              */

              for(i=0;i<THEMES.length;i++){
                theme = THEMES[i];
                gulp.src( './src/ad.html' )
                  .pipe(replace( ' id="DEFAULT_THEME"',' id="theme-'+theme+'"'))
                  .pipe( inlineCss({
                    removeHtmlSelectors:0,//TODO
                    //url:'',
                    preserveMediaQueries:false,
                    applyWidthAttributes:true,
                    applyTableAttributes:true,
                      applyStyleTags:  true,
                      removeStyleTags: false,
                      applyLinkTags:   true,
                      removeLinkTags:  true,
                      extraCss: css
                   }))
                   .pipe( rename( 'index-'+theme+'.html' ) )
                   .pipe( gulp.dest( './export/' ) );
              }
          });

});

gulp.task( 'build-export', function() {
    return sequence( 'export-html' );
    //return gulp.src('./build/ad.html')
    //.pipe( gulp.dest( './export' ) );
    //TODO: teaser.txt, logo.png, images/*
});


gulp.task( 'build-preview', function(cb) {

  gutil.log('[build-preview] ... ');

    /*gulp.src( './export/index.html' )
        //.pipe( data( require( './preview/example-data.js' ) ) ) // provide context data
        //.pipe( swig( { defaults: { cache: false } } ) )  // turn off Swig caching
        .pipe( rename( 'ad.html' ) )
        .pipe( gulp.dest( './preview' ) );*/

        /*
        WORKING w/o themes:

  return gulp.src('./export/index.html')
  .on('data', function(file) {
    var adHtml = file.contents.toString();
    return gulp.src('./preview/index-template.html')
      .pipe(replace( '<!--#include AD HTML -->',adHtml))
      //.pipe( htmlincludes() )
      .pipe( rename( 'index.html' ) )
      .pipe( gulp.dest( './preview' ) )
      .pipe( bs.reload( { stream: true } ) )
      .on('end', cb);
      });
      */

      //TODO: only get index-* within THEMES[],
      //TODO: or discard old index-* wihtin ./export/
      // for(i=0;i<THEMES.length;i++){ theme = THEMES[i];

      var html = [];
      var i = 0;
      function appendTheme(){
          theme = THEMES[i++];
          if( ! theme ){
            gutil.log('[build-preview] Did all themes; compiling preview.');
            return gulp.src('./preview/index-template.html')
              .pipe(replace( '<!--#include AD HTML -->', html.join('<hr class="ad-variant-separator" />') ))
              //.pipe( htmlincludes() )
              .pipe( rename( 'index.html' ) )
              .pipe( gulp.dest( './preview' ) )
              .pipe( bs.reload( { stream: true } ) )
              .on('end', cb);
          };
            gutil.log('[build-preview] THEME:'+theme+' ... ');

          gulp.src('./export/index-'+theme+'.html')
            .on('data', function(file) {
              gutil.log('[build-preview] on.data for theme: '+theme+' ...');
              //  gutil.log( file.contents.toString() )
              var adHtml = file.contents.toString();
              html.push( adHtml );
              appendTheme();
            });
      }
appendTheme();
return;

for(i=0;i<THEMES.length;i++){
  gutil.log('[build-preview] THEME:'+theme+' ... ');

}

return gulp.src('./preview/index-template.html')
  .pipe(replace( '<!--#include AD HTML -->', html.join('<hr />') ))
  //.pipe( htmlincludes() )
  .pipe( rename( 'index.html' ) )
  .pipe( gulp.dest( './preview' ) )
  .pipe( bs.reload( { stream: true } ) );

return;


return
gulp.src('./export/index-a-light.html')

.pipe(data(function(file) {
  gutil.log('[build-preview] on data ...');
  return file;
}))

.on('data', function(file) {
  gutil.log('[build-preview] on data ...');
  return file;
    //gutil.log( file.contents.toString() )
  })
    .pipe(concat('index-_ALL_.html'))
    .on('data', function(file) {
      gutil.log('[build-preview] on data ...');
        gutil.log( file.contents.toString() )
      var adHtml = file.contents.toString();
      return gulp.src('./preview/index-template.html')
        .pipe(replace( '<!--#include AD HTML -->',adHtml))
        //.pipe( htmlincludes() )
        .pipe( rename( 'index.html' ) )
        .pipe( gulp.dest( './preview' ) )
        .pipe( bs.reload( { stream: true } ) )
        .on('end', cb);
        });




});

// Task: Start server and watchers
gulp.task( 'watch', function() {

      gulp.watch( './src/styles.scss', [ 'build-export' ] )
        .on('change', bs.reload);
      gulp.watch( './src/ad.html', [ 'build-export' ] )
        .on('change', bs.reload);
      gulp.watch( './export/index-*.html', [ 'build-preview' ] )
        .on('change', bs.reload);
      gulp.watch( './preview/index-template.html', [ 'build-preview' ] )
        .on('change', bs.reload);

      gulp.watch( './preview/index.html' ).on('change', bs.reload);
      return;

   gulp.watch([
     './export/index-*.html',
     './preview/index-template.html']
   ['build-preview','serve-refresh']);

    gulp.watch([
      './src/styles.scss',
      './src/ad.html']
    ['export-html']);

   return;
    gulp.watch( './src/styles.scss', [ 'export-and-preview' ] );
    gulp.watch( './src/ad.html', [ 'export-and-preview' ] );
});

gulp.task( 'serve', function() {
    bs.init( { server: "./preview" } );   // server is at http://localhost:3000
});
gulp.task( 'serve-refresh', function() {
    bs.reload( { stream: false } );   // server is at http://localhost:3000
});

gulp.task( 'preview', function() {
    return sequence( 'build-preview', 'serve' );
});

gulp.task( 'build', function() {
    return sequence( 'build-export', 'build-preview' );
});


gulp.task( 'send-test-email', function() {
  // Define constants (edit per your settings)
  var SERVICE = 'Gmail'
      AUTH    = {
          user: 'info@seerinteractive.com', // your sender Gmail acct
          pass: 'OIU%V*^$#Cw2XRyY'             // your Gmail password
      },
      FROM    = AUTH.user,
      SUBJECT = 'Gulp Build Test (Nodemailer)';

  // Define nodemailer options object
  var mailOpts = {
      from:    FROM,
      subject: SUBJECT,
      to:      'stephenh@seerinteractive.com',//process.argv.slice( 2 )[0],
      html:    fs.readFileSync( './export/index-*.html', 'utf8' )
  };

  // Create nodemailer "transporter" object
  var transport = mailer.createTransport({
      service: SERVICE,
      auth:    AUTH
  });

  // Fire off email and display response
  transport.sendMail( mailOpts, function( error, info ) {
      error ? console.log( error ) : console.log( 'Sent: ' + info.response );
  });

});


gulp.task( 'export-and-send-test-email', function() {
    return sequence( 'build-export', 'send-test-email' );
});

// Task: Default (run everything once, in sequence, and start server)
gulp.task( 'default', function() {
    sequence( 'build-export', 'build-preview', 'watch', 'serve' );
});
