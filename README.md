# SEER TODO DESIGN ISSUES


just as limited as email template
 - no custom fonts, except what's on user's computer
 - no hover/focus effects ??



search for: ISSUE_IMGLVSR
Designs a vs b: img on left vs right
- how should stack on mobile?
- only template diff that warrants diff html (maybe?)


theme d:
- is there a paoint to giving a single purple color variant if e don't offer a custom color?






# Usage INstructions

The HTML folder for the Video template includes a single HTML file and several images in an
‘images’ folder.

## Add logo img

The file must be named exactly “logo.png,” “logo.jpg,” or “logo.gif” and the file format must be PNG, JPG, or GIF.

Logo image size: minimum 144px x 144px with aspect ratio 1:1
Maximum file size: 150KB
File formats: PNG, JPG, or GIF (non-animated)
The logo image is a small icon, graphic or brand logo.

The image will be displayed as a 50 x 50 square on normal resolution devices, but we accept up to 150KB for clearer viewing on some screens.

# Add images

Image folder specifications
Your ad must contain at least 1 image, and can contain up to 100 images. To embed videos in your expanded ad, jump to the video section below.
Max width per image: 650px
Max height per image: 1000px (Note: For the most effective ads, we recommend of no more than 650px height for the total ad canvas, which includes all your text and images.)


## Adding Clickthrough URLs
TODO


## Upload

zip up the contents (not the folder, bt everythign inside the folder). https://screencast.com/t/JEn8acpW7W9

Previewing in AdWords
Please refer to Help Center article for complete instructions.
https://support.google.com/displayspecs/answer/7019461?visit_id=0-636453313260065268-1475348730&rd=1





# Dev Guidelines

https://support.google.com/displayspecs/answer/7019461

Spec restrictions
Max Width: 650px
Height: 1000px (recommend no more than 650px)
Recommend use of Table to ensure consistent presentation on multiple browsers
Allowed Markup:
● HTML
● No Javascript (inline or linked .js files)
● No Stylesheets (style attribute of tags is okay though)
● No Flash, HTML5, Audio, iFrames or animated images (including animated GIFs)
● Some CSS is not supported. See below for further details.


Validator
https://h5validator.appspot.com/gmail/asset
