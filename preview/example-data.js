module.exports = {
    ASSETS_PATH: '/images/',
    name:        'Jane Doe',
    message:     'This is an amazing offer for:',
    offers:      [
        { name: 'Product 1', image: 'prod1.jpg' },
        { name: 'Product 2', image: 'prod2.jpg' }
    ]
};
